function selectionSort(input) {
    let min = 0;
    let idx = 0;
    for(let i = 0; i < input.length; i++) {
        min = input[i];
        idx = i;
        for(let j = i+1; j < input.length; j++) {
            if(min > input[j])
            {
                idx = j;
                min = input[j];
            }
        }
        let temp = input[i];
        input[i] = min;
        input[idx] = temp;
    }

    console.log(input);
}

selectionSort([6,4,7,1,2,3,-12,10,4]);